# Where do history professors get their PhDs?

[Visualization](https://plot.ly/~DBaum1/3)

Inspired by [this](https://advances.sciencemag.org/content/1/1/e1400005/tab-figures-data)
study, I decided to try to replicate the results (albeit with a considerably smaller 
pool of universities). I was interested in two things: the distribution of history 
PhDs across surveyed university faculties and within faculties. I also figured it 
would be a good opportunity to learn the fundamentals of web scraping.

# Methodology

I used the [Association of American Universities](https://en.wikipedia.org/wiki/Association_of_American_Universities)
as a guide. Every AAU member except for Purdue and the two Canadian universities 
(sorry Canadians!) were analyzed, for 60 universities total. Dartmouth and Tufts are two 
universities that were also included, even though neither are AAU members.

About a third were done by hand. JD's, medical degrees, and habilitations were 
not counted. The rest were scraped. Universities that were done by hand did not 
typically count adjuncts - in hindsight, this may have been a mistake as my spiders 
did not differentiate between tenure and non tenure track faculty.

# Results 

The average number of history professors produced by a university was 13, while 
the median was 2. Approximately 53% of history professors at the surveyed 
universities came from just 9 universities:

Rank  | University
----- | -------------
1     | Yale 
2     | Harvard
3     | Berkeley
4     | Columbia 
5     | UChicago 
6     | Princeton
7     | Stanford
8     | UMich 
9     | Wisconsin 

I was pleased to find that my (much less rigorous) data analysis generally 
mimicked the [findings](https://advances.sciencemag.org/content/1/1/e1400005/tab-figures-data) 
of the study. Their rankings were:

Rank  | University
----- | -------------
1     | Harvard 
2     | Yale
3     | Berkeley
4     | Princeton 
5     | Stanford 
6     | UChicago
7     | Columbia
8     | Brandeis 
9     | Johns Hopkins 

While ranks have shifted around a bit, the only significant difference in the rank 
of the top universities is my inclusion of the University of Michigan, Ann Arbor 
and the University of Wisconsin–Madison is the top 9. My analysis places Johns 
Hopkins University at #11, just out of the top 10 history faculty producing 
universities. Coincidentally, the study placed Wisconsin at #11 (they apparently 
just switched places). Brandeis is significantly different in my ranking, at #26, 
with "only" 22 professors - tied with Brown University. 

Within my dataset, the University of Cambridge and the University of Oxford were 
indisuputably the best performing foreign universities, followed much further down 
by the University of Paris (Sorbonne).

Comparison of the individual results indicates that my rankings were 
more generous to public universities than the study's.

Regardless of the differences, the final conclusion remains the same: A tiny 
number of universities produce the overwhelming majority of history professors. 
This is concentrated in the "top 30" professor producing institutions, with 
the top 10 taking the lion's share of that. History students who wish to maximize 
their chances of gaining a professorship would do well to focus on pursuing their 
PhD at one of the following universities:

* Harvard University
* Yale University
* UC Berkeley
* Princeton University
* Stanford University
* University of Chicago
* Columbia University
* Brandeis University
* Johns Hopkins University
* University of Pennsylvania
* University of Wisconsin, Madison
* University of Michigan
* UCLA
* Northwestern University
* Cornell University
* Brown University
* UC Davis
* University of Rochester
* New York University
* UC San Diego
* Duke University
* University of Minnesota, Minneapolis
* Rutgers University
* University of North Carolina, Chapel Hill
* University of Virginia
* University of Southern California
* University of Washington
* MIT
* University of Texas, Austin
* Emory University

# Tech/frameworks used

* [Jupyter Notebook](https://jupyter.org/)
* [Scrapy](https://scrapy.org/)
* [Plotly](https://plot.ly/python/)

# License
[MIT](https://choosealicense.com/licenses/mit/)
