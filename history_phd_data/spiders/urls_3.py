# -*- coding: utf-8 -*-
import scrapy
from history_phd_data.items import HistoryPhdDataItem

class Urls3Spider(scrapy.Spider):
    name = 'urls_3'
    start_urls = ['https://history.mit.edu/people',
                  'https://www.history.ucsb.edu/directory/faculty/',
                  'https://www.history.northwestern.edu/people/faculty/core-faculty/',
                  'https://history.stanford.edu/people/faculty',
                  'https://www.stonybrook.edu/commcms/history/people/faculty/',
                  'https://liberalarts.tulane.edu/departments/history/people/faculty-staff-by-name'
                 ]
    
    def parse(self, response):
        prof = HistoryPhdDataItem()
        if response.url == 'https://history.mit.edu/people':
            prof['curr_university'] = 'Massachusetts Institute of Technology'
            hrefs = response.xpath("//*[@class='views-field views-field-title']//@href").extract()
            for href in hrefs:
                url = response.urljoin(href)
                request = scrapy.Request(url, callback=self.parse_page)
                request.meta['prof'] = prof
                yield request
        elif response.url == 'https://www.history.ucsb.edu/directory/faculty/':
            prof['curr_university'] = 'University of California, Santa Barbara'
            hrefs = response.xpath("//*[@class='directory-name']//@href").extract()
            for href in hrefs:
                url = response.urljoin(href)
                request = scrapy.Request(url, callback=self.parse_page)
                request.meta['prof'] = prof
                yield request
        elif response.url == 'https://www.history.northwestern.edu/people/faculty/core-faculty/':
            prof['curr_university'] = 'Northwestern University'
            hrefs = response.xpath("//h3//@href").extract()
            for href in hrefs:
                url = response.urljoin(href)
                request = scrapy.Request(url, callback=self.parse_nw_page)
                request.meta['prof'] = prof
                yield request
        elif response.url == 'https://history.stanford.edu/people/faculty':
            prof['curr_university'] = 'Stanford University'
            hrefs = response.xpath("//h3//@href").extract()
            for href in hrefs:
                url = response.urljoin(href)
                request = scrapy.Request(url, callback=self.parse_page)
                request.meta['prof'] = prof
                yield request
        elif response.url == 'https://www.stonybrook.edu/commcms/history/people/faculty/':
            prof['curr_university'] = 'State University of New York at Stony Brook'
            hrefs = response.xpath("//*[@class='view-link sbu-button fa-arrow-after']//@href").extract()
            for href in hrefs:
                url = response.urljoin(href)
                request = scrapy.Request(url, callback=self.parse_sbu_page)
                request.meta['prof'] = prof
                yield request
        else:
            prof['curr_university'] = 'Tulane University'
            hrefs = response.xpath("//div[@class='panel-pane pane-views-panes pane-staff-members-faculty-history']//div[@class='staff_teaser--image']//@href").extract()
            for href in hrefs:
                url = response.urljoin(href)
                request = scrapy.Request(url, callback=self.parse_page)
                request.meta['prof'] = prof
                yield request

    def parse_page(self, response):
        prof = response.meta['prof']    
        prof['url'] = response.url
        phd = response.xpath("//text()[starts-with(., 'Ph.D') or starts-with(., 'Ph. D') \
                             or starts-with(., 'Doctorat') or starts-with(., 'DPhil') \
                             or starts-with(., 'PhD') or starts-with(., '\nPh. D') \
                             or starts-with(., '\nPhD') or starts-with(., ' Ph.D') \
                             or starts-with(., '\nPh.D') or starts-with(., 'Ph.D') \
                             or starts-with(., 'D. Phil') or starts-with(., '\n      PhD') \
                             or starts-with(., '\n      Ph.D') or starts-with(., '(Ph.D') \
                             or starts-with(., '\n      DPhil') or starts-with(., 'D.Phil') \
                             or starts-with(., 'Doctor of Philosophy') \
                             or starts-with(., '\n      Dr. phil.') \
                             or starts-with(., '\nDr. Phil.')]").extract()
        phd = phd[0].strip()
        prof['phd'] = phd
        yield prof
        
    def parse_nw_page(self, response):
        prof = response.meta['prof']    
        prof['url'] = response.url
        phd = response.xpath("//text()[starts-with(., 'Ph.D') or starts-with(., 'Ph. D') \
                             or starts-with(., 'Doctorat') or starts-with(., 'DPhil') \
                             or starts-with(., 'PhD') or starts-with(., '\nPh. D') \
                             or starts-with(., '\nPhD') or starts-with(., ' Ph.D') \
                             or starts-with(., '\nPh.D') or starts-with(., 'Ph.D') \
                             or starts-with(., 'D. Phil') or starts-with(., '\n      PhD') \
                             or starts-with(., '\n      Ph.D') or starts-with(., '(Ph.D') \
                             or starts-with(., '\n      DPhil') or starts-with(., 'D.Phil') \
                             or starts-with(., 'Doctor of Philosophy') \
                             or starts-with(., '\n      Dr. phil.') \
                             or starts-with(., '\nDr. Phil.')]")[1].extract()
        phd = phd.strip()
        prof['phd'] = phd
        yield prof

    def parse_sbu_page(self, response):
        prof = response.meta['prof']   
        prof['url'] = response.url
        phd = response.xpath("//text()[starts-with(., 'Professor') \
                or starts-with(., 'Associate Professor') \
                or starts-with(., 'Lecturer') \
                or starts-with(., 'Assistant Professor') \
                or starts-with(., 'Distinguished Professor')]").extract()
        phd = phd[0].strip()
        prof['phd'] = phd
        yield prof
