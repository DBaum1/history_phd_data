# -*- coding: utf-8 -*-
import scrapy
from history_phd_data.items import HistoryPhdDataItem

class JhuspiderSpider(scrapy.Spider):
    name = 'JHUSpider'
    start_urls = ['https://history.jhu.edu/people/']

    def parse(self, response):
        for item in response.xpath("//*[@class='media-object-section']"):
            prof = HistoryPhdDataItem()
            prof['url'] = item.xpath("h3/a/@href").extract()
            prof['curr_university'] = 'Johns Hopkins University'
            prof['phd'] = item.xpath("h5//text()").extract()
            yield prof