# -*- coding: utf-8 -*-
import scrapy
from history_phd_data.items import HistoryPhdDataItem

class UmichspiderSpider(scrapy.Spider):
    name = 'UMichSpider'
    start_urls = [
                    'https://lsa.umich.edu/history/people/faculty/alvita-akiboh.html',
                    'https://lsa.umich.edu/history/people/faculty/palberto.html',
                    'https://lsa.umich.edu/history/people/faculty/hakem-al-rustom.html',
                    'https://lsa.umich.edu/history/people/faculty/babayan.html',
                    'https://lsa.umich.edu/history/people/faculty/pballing.html',
                    'https://lsa.umich.edu/history/people/faculty/sberrey.html',
                    'https://lsa.umich.edu/history/people/faculty/hbrick.html',
                    'https://lsa.umich.edu/history/people/faculty/jscarson.html',
                    'https://lsa.umich.edu/history/people/faculty/cassel.html',
                    'https://lsa.umich.edu/history/people/faculty/scaul.html',
                    'https://lsa.umich.edu/history/people/faculty/cschang.html',
                    'https://lsa.umich.edu/history/people/faculty/rchin.html',
                    'https://lsa.umich.edu/history/people/faculty/ecipa.html',
                    'https://lsa.umich.edu/history/people/faculty/joshcole.html',
                    'https://lsa.umich.edu/history/people/faculty/jrcole.html',
                    'https://lsa.umich.edu/history/people/faculty/jwcook.html',
                    'https://lsa.umich.edu/history/people/faculty/mcountry.html',
                    'https://lsa.umich.edu/history/people/faculty/henry-cowles.html',
                    'https://lsa.umich.edu/history/people/faculty/ddmoore.html',
                    'https://lsa.umich.edu/history/people/faculty/ddelac.html',
                    'https://lsa.umich.edu/history/people/faculty/cdepee.html',
                    'https://lsa.umich.edu/history/people/faculty/dowdg.html',
                    'https://lsa.umich.edu/history/people/faculty/ghe.html',
                    'https://lsa.umich.edu/history/people/faculty/fancy.html',
                    'https://lsa.umich.edu/history/people/faculty/folland.html',
                    'https://lsa.umich.edu/history/people/faculty/anna-friedin.html',
                    'https://lsa.umich.edu/history/people/faculty/frenchk.html',
                    'https://lsa.umich.edu/history/people/faculty/dariog.html',
                    'https://lsa.umich.edu/history/people/faculty/wglover.html',
                    'https://lsa.umich.edu/history/people/faculty/hancockd.html',
                    'https://lsa.umich.edu/history/people/faculty/cchawes.html',
                    'https://lsa.umich.edu/history/people/faculty/jessehg.html',
                    'https://lsa.umich.edu/history/people/faculty/jhowell.html',
                    'https://lsa.umich.edu/history/people/faculty/kisrael.html',
                    'https://lsa.umich.edu/history/people/faculty/raevin-jimenez.html',
                    'https://lsa.umich.edu/history/people/faculty/paulcjoh.html',
                    'https://lsa.umich.edu/history/people/faculty/jennifer-jones.html',
                    'https://lsa.umich.edu/history/people/faculty/sjuster.html',
                    'https://lsa.umich.edu/history/people/faculty/pkazanji.html',
                    'https://lsa.umich.edu/history/people/faculty/mckelley.html',
                    'https://lsa.umich.edu/history/people/faculty/vkivelso.html',
                    'https://lsa.umich.edu/history/people/faculty/langland.html',
                    'https://lsa.umich.edu/history/people/faculty/mlassite.html',
                    'https://lsa.umich.edu/history/people/faculty/earl-lewis.html',
                    'https://lsa.umich.edu/history/people/faculty/eurasia.html',
                    'https://lsa.umich.edu/history/people/faculty/jmarwil.html',
                    'https://lsa.umich.edu/history/people/faculty/masuzawa.html',
                    'https://lsa.umich.edu/history/people/faculty/tmcd.html',
                    'https://lsa.umich.edu/history/people/faculty/millsken.html',
                    'https://lsa.umich.edu/history/people/faculty/fmir.html',
                    'https://lsa.umich.edu/history/people/faculty/apmora.html',
                    'https://lsa.umich.edu/history/people/faculty/ianmoyer.html',
                    'https://lsa.umich.edu/history/people/faculty/emuehlbe.html',
                    'https://lsa.umich.edu/history/people/faculty/rneis.html',
                    'https://lsa.umich.edu/history/people/faculty/northrop.html',
                    'https://lsa.umich.edu/history/people/faculty/mpernick.html',
                    'https://lsa.umich.edu/history/people/faculty/drpeters.html',
                    'https://lsa.umich.edu/history/people/faculty/baporter.html',
                    'https://lsa.umich.edu/history/people/faculty/ellen-poteet.html',
                    'https://lsa.umich.edu/history/people/faculty/puffh.html',
                    'https://lsa.umich.edu/history/people/faculty/rjscott.html',
                    'https://lsa.umich.edu/history/people/faculty/pselcer.html',
                    'https://lsa.umich.edu/history/people/faculty/ian-shin.html',
                    'https://lsa.umich.edu/history/people/faculty/lakisha-simmons.html',
                    'https://lsa.umich.edu/history/people/faculty/sinha.html',
                    'https://lsa.umich.edu/history/people/faculty/spec.html',
                    'https://lsa.umich.edu/history/people/faculty/matthew-spooner.html',
                    'https://lsa.umich.edu/history/people/faculty/pasqua.html',
                    'https://lsa.umich.edu/history/people/faculty/amstern.html',
                    'https://lsa.umich.edu/history/people/faculty/rgsuny.html',
                    'https://lsa.umich.edu/history/people/faculty/meltan.html',
                    'https://lsa.umich.edu/history/people/faculty/hthompsn.html',
                    'https://lsa.umich.edu/history/people/faculty/thurmank.html',
                    'https://lsa.umich.edu/history/people/faculty/tomitono.html',
                    'https://lsa.umich.edu/history/people/faculty/jveidlin.html',
                    'https://lsa.umich.edu/history/people/faculty/mwitgen.html',
                    'https://lsa.umich.edu/history/people/faculty/mwroblew.html',
                    'https://lsa.umich.edu/history/people/faculty/yi-li-wu.html',
                    'https://lsa.umich.edu/history/people/faculty/jason-young.html'
                  ]

    def parse(self, response):
        prof = HistoryPhdDataItem()
        prof['url'] = response.url
        prof['curr_university'] = 'University of Michigan'
        prof['phd'] = response.xpath("//p//text()[starts-with(., 'Ph.D') or starts-with(., 'Ph. D') \
                             or starts-with(., 'Doctorat') or starts-with(., 'DPhil') \
                             or starts-with(., 'PhD') or starts-with(., '\nPh. D') \
                             or starts-with(., '\nPhD') or starts-with(., ' Ph.D') \
                             or starts-with(., '\nPh.D') or starts-with(., 'Ph.D') \
                             or starts-with(., 'D. Phil') or starts-with(., '\n      PhD') \
                             or starts-with(., '\n      Ph.D') or starts-with(., '(Ph.D') \
                             or starts-with(., '\n      DPhil') or starts-with(., 'D.Phil') \
                             or starts-with(., 'Doctor of Philosophy') \
                             or starts-with(., '\n      Dr. phil.')]").extract()
        yield prof
