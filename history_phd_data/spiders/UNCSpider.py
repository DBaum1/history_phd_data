# -*- coding: utf-8 -*-
import scrapy
from history_phd_data.items import HistoryPhdDataItem

class UncspiderSpider(scrapy.Spider):
    name = 'UNCSpider'
    start_urls = ['https://history.unc.edu/faculty/']

    def parse(self, response):
        hrefs = response.xpath("//tbody//tr//td[2]//@href").extract()
        for href in hrefs:
            prof = HistoryPhdDataItem()
            url = response.urljoin(href)
            #go to prof page
            request = scrapy.Request(url, callback=self.parse_page)
            request.meta['prof'] = prof
            yield request

    def parse_page(self, response):
        prof = response.meta['prof']    
        prof['url'] = response.url
        prof['curr_university'] = 'University of North Carolina at Chapel Hill'
        phd = response.xpath("//text()[starts-with(., 'Ph.D') or starts-with(., 'Ph. D') \
                             or starts-with(., 'Doctorat') or starts-with(., 'DPhil') \
                             or starts-with(., 'PhD') or starts-with(., '\nPh. D') \
                             or starts-with(., '\nPhD') or starts-with(., ' Ph.D') \
                             or starts-with(., '\nPh.D') or starts-with(., 'Ph.D') \
                             or starts-with(., 'D. Phil') or starts-with(., '\n      PhD') \
                             or starts-with(., '\n      Ph.D') or starts-with(., '(Ph.D') \
                             or starts-with(., '\n      DPhil') or starts-with(., 'D.Phil') \
                             or starts-with(., 'Doctor of Philosophy') \
                             or starts-with(., '\n      Dr. phil.') \
                             or starts-with(., '\nDr. Phil.')]")[1].extract()
        phd = phd.strip()
        prof['phd'] = phd
        yield prof
        pass