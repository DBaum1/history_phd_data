# -*- coding: utf-8 -*-
import scrapy
from history_phd_data.items import HistoryPhdDataItem

class UwspiderSpider(scrapy.Spider):
    name = 'UWSpider'
    start_urls = ['https://history.washington.edu/people/faculty?name=&interests=All&order=field_email&sort=desc']

    def parse(self, response):
        hrefs = response.xpath('//h3//@href').extract()
        for href in hrefs:
            prof = HistoryPhdDataItem()
            url = response.urljoin(href)
            prof['url'] = url
            #go to prof page
            request = scrapy.Request(url, callback=self.parse_page)
            request.meta['prof'] = prof
            yield request

    def parse_page(self, response):
        prof = response.meta['prof']    
        prof['curr_university'] = 'University of Washington'
        phd = response.xpath("//text()[starts-with(., 'Ph.D') or starts-with(., 'Ph. D') \
                             or starts-with(., 'Doctorat') or starts-with(., 'DPhil') \
                             or starts-with(., 'PhD') or starts-with(., '\nPh. D') \
                             or starts-with(., '\nPhD') or starts-with(., ' Ph.D') \
                             or starts-with(., '\nPh.D') or starts-with(., 'Ph.D') \
                             or starts-with(., 'D. Phil') or starts-with(., '\n      PhD') \
                             or starts-with(., '\n      Ph.D') or starts-with(., '(Ph.D') \
                             or starts-with(., '\n      DPhil') or starts-with(., 'D.Phil') \
                             or starts-with(., 'Doctor of Philosophy') \
                             or starts-with(., '\n      Dr. phil.')]")[3].extract()
        phd = phd.strip()
        prof['phd'] = phd
        yield prof