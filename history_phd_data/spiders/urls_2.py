# -*- coding: utf-8 -*-
import scrapy
from history_phd_data.items import HistoryPhdDataItem

class Urls2Spider(scrapy.Spider):
    name = 'urls_2'
    start_urls = ['https://history.indiana.edu/faculty_staff/index.html',
                  'https://history.dartmouth.edu/people',
                  'http://history.emory.edu/home/people/faculty/index.html',
                  'https://history.la.psu.edu/directory/faculty',
                  'http://ase.tufts.edu/history/faculty/',
                  'https://history.illinois.edu/directory/faculty',
                  'http://www.history.pitt.edu/people/faculty'
                  ]
    
    def parse(self, response):
        prof = HistoryPhdDataItem()
        if response.url == 'https://history.indiana.edu/faculty_staff/index.html':
            prof['curr_university'] = 'Indiana University Bloomington'
            hrefs = response.xpath("//section[@class='core collapsed bg-none section']//h1//@href").extract()
            for href in hrefs:
                url = response.urljoin(href)
                request = scrapy.Request(url, callback=self.parse_page)
                request.meta['prof'] = prof
                yield request
        elif response.url == 'https://history.dartmouth.edu/people':
            prof['curr_university'] = 'Dartmouth College'
            hrefs = response.xpath("//*[@class='node-person node-page-listing']//h3//@href").extract()
            for href in hrefs:
                url = response.urljoin(href)
                request = scrapy.Request(url, callback=self.parse_page)
                request.meta['prof'] = prof
                yield request
        elif response.url == 'http://history.emory.edu/home/people/faculty/index.html':
            prof['curr_university'] = 'Emory University'
            hrefs = response.xpath("//h3//@href").extract()
            for href in hrefs:
                url = response.urljoin(href)
                request = scrapy.Request(url, callback=self.parse_page)
                request.meta['prof'] = prof
                yield request
        elif response.url == 'https://history.la.psu.edu/directory/faculty':
            prof['curr_university'] = 'Pennsylvania State University'
            hrefs = response.xpath("//*[@class='personName']//@href").extract()
            for href in hrefs:
                url = response.urljoin(href)
                request = scrapy.Request(url, callback=self.parse_page)
                request.meta['prof'] = prof
                yield request
        elif response.url == 'http://ase.tufts.edu/history/faculty/':
            prof['curr_university'] = 'Tufts University'
            hrefs = response.xpath("//table[@id='tablegrid'][1]//@href").extract()
            for href in hrefs:
                url = response.urljoin(href)
                request = scrapy.Request(url, callback=self.parse_page)
                request.meta['prof'] = prof
                yield request
        elif response.url == 'https://history.illinois.edu/directory/faculty':
            prof['curr_university'] = 'University of Illinois at Urbana–Champaign'
            hrefs = response.xpath("//*[@class='directory__name-link']//@href").extract()
            for href in hrefs:
                url = response.urljoin(href)
                request = scrapy.Request(url, callback=self.parse_page)
                request.meta['prof'] = prof
                yield request
        else:
            prof['curr_university'] = 'University of Pittsburgh'
            hrefs = response.xpath("//div[@class='overallcontent']//@href").extract()
            for href in hrefs:
                url = response.urljoin(href)
                request = scrapy.Request(url, callback=self.parse_page)
                request.meta['prof'] = prof
                yield request

                
    def parse_page(self, response):
        prof = response.meta['prof']    
        prof['url'] = response.url
        phd = response.xpath("//li//text()[starts-with(., 'Ph.D') or starts-with(., 'Ph. D') \
                             or starts-with(., 'Doctorat') or starts-with(., 'DPhil') \
                             or starts-with(., 'PhD') or starts-with(., '\nPh. D') \
                             or starts-with(., '\nPhD') or starts-with(., ' Ph.D') \
                             or starts-with(., '\nPh.D') or starts-with(., 'Ph.D') \
                             or starts-with(., 'D. Phil') or starts-with(., '\n      PhD') \
                             or starts-with(., '\n      Ph.D') or starts-with(., '(Ph.D') \
                             or starts-with(., '\n      DPhil') or starts-with(., 'D.Phil') \
                             or starts-with(., 'Doctor of Philosophy') \
                             or starts-with(., '\n      Dr. phil.') \
                             or starts-with(., '\nDr. Phil.')]").extract()
        phd = phd[0].strip()
        prof['phd'] = phd
        yield prof