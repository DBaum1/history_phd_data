# -*- coding: utf-8 -*-
import scrapy
from history_phd_data.items import HistoryPhdDataItem

class Test2Spider(scrapy.Spider):
    name = 'test2'
    start_urls = ['https://liberalarts.utexas.edu/history/faculty/'
                  ]
    
    def parse(self, response):
        prof = HistoryPhdDataItem()
        prof['curr_university'] = 'University of Texas at Austin'
        hrefs = response.xpath("//*[@class='small-8 medium-9 large-10 columns faculty-contact-info']//h3//@href").extract()
        for href in hrefs:
            url = response.urljoin(href)
            request = scrapy.Request(url, callback=self.parse_page)
            request.meta['prof'] = prof
            yield request

    def parse_page(self, response):
        prof = response.meta['prof']    
        prof['url'] = response.url
        phd = response.xpath("//h3//span[@itemprop='alumniOf']//text()").extract()
        phd = phd[0].strip()
        prof['phd'] = phd
        yield prof

