# -*- coding: utf-8 -*-
import scrapy
from history_phd_data.items import HistoryPhdDataItem

class Urls1Spider(scrapy.Spider):
    name = 'urls_1'
    start_urls = ['https://history.ucla.edu/faculty',
                  'https://history.uchicago.edu/directories/full/current-faculty',
                  'https://history.berkeley.edu/people/faculty',
                  'https://history.wisc.edu/people-main/faculty-listed-alphabetically/',
                  'https://history.duke.edu/people/faculty',
                  'http://www.sas.rochester.edu/his/people/faculty/index.html',
                  'https://history.virginia.edu/faculty',
                  'https://www.colorado.edu/history/faculty/full-time-faculty',
                  'https://as.vanderbilt.edu/history/people/index.php?group=faculty',
                  'https://history.missouri.edu/people'
                  ]
    
    def parse(self, response):
        prof = HistoryPhdDataItem()
        if response.url == 'https://history.ucla.edu/faculty':
            prof['curr_university'] = 'University of California, Los Angeles'
            hrefs = response.xpath("//h1//@href").extract()
            for href in hrefs:
                url = response.urljoin(href)
                request = scrapy.Request(url, callback=self.parse_page)
                request.meta['prof'] = prof
                yield request
        elif response.url == 'https://history.uchicago.edu/directories/full/current-faculty':
            prof['curr_university'] = 'University of Chicago'
            hrefs = response.xpath("//h2//@href").extract()
            for href in hrefs:
                url = response.urljoin(href)
                request = scrapy.Request(url, callback=self.parse_page)
                request.meta['prof'] = prof
                yield request
        elif response.url == 'https://history.berkeley.edu/people/faculty':
            prof['curr_university'] = 'University of California, Berkeley'
            hrefs = response.xpath("//h3//@href").extract()
            for href in hrefs:
                url = response.urljoin(href)
                request = scrapy.Request(url, callback=self.parse_page)
                request.meta['prof'] = prof
                yield request
        elif response.url == 'https://history.wisc.edu/people-main/faculty-listed-alphabetically/':
            prof['curr_university'] = 'University of Wisconsin–Madison'
            hrefs = response.xpath("//h3//@href").extract()
            for href in hrefs:
                url = response.urljoin(href)
                request = scrapy.Request(url, callback=self.parse_page)
                request.meta['prof'] = prof
                yield request
        elif response.url == 'https://history.duke.edu/people/faculty':
            prof['curr_university'] = 'Duke University'
            hrefs = response.xpath("//h4//@href").extract()
            for href in hrefs:
                url = response.urljoin(href)
                request = scrapy.Request(url, callback=self.parse_page)
                request.meta['prof'] = prof
                yield request
        elif response.url == 'http://www.sas.rochester.edu/his/people/faculty/index.html':
            prof['curr_university'] = 'University of Rochester'
            hrefs = response.xpath("//h4//@href").extract()
            for href in hrefs:
                url = response.urljoin(href)
                request = scrapy.Request(url, callback=self.parse_page)
                request.meta['prof'] = prof
                yield request
        elif response.url == 'https://history.virginia.edu/faculty':
            prof['curr_university'] = 'University of Virginia'
            hrefs = response.xpath("//h4//@href").extract()
            for href in hrefs:
                url = response.urljoin(href)
                request = scrapy.Request(url, callback=self.parse_page)
                request.meta['prof'] = prof
                yield request
        elif response.url == 'https://www.colorado.edu/history/faculty/full-time-faculty':
            prof['curr_university'] = 'University of Colorado Boulder'
            hrefs = response.xpath("//td//strong//@href").extract()
            for href in hrefs:
                url = response.urljoin(href)
                request = scrapy.Request(url, callback=self.parse_page)
                request.meta['prof'] = prof
                yield request
        elif response.url == 'https://as.vanderbilt.edu/history/people/index.php?group=faculty':
            prof['curr_university'] = 'Vanderbilt University'
            hrefs = response.xpath("//td//strong//@href").extract()
            for href in hrefs:
                url = response.urljoin(href)
                request = scrapy.Request(url, callback=self.parse_page)
                request.meta['prof'] = prof
                yield request
        else:
            prof['curr_university'] = 'University of Missouri'
            hrefs = response.xpath("//*[@class='views-field views-field-title']//@href").extract()
            for href in hrefs:
                url = response.urljoin(href)
                request = scrapy.Request(url, callback=self.parse_page)
                request.meta['prof'] = prof
                yield request
                
    def parse_page(self, response):
        prof = response.meta['prof']    
        prof['url'] = response.url
        phd = response.xpath("//p//text()[starts-with(., 'Ph.D') or starts-with(., 'Ph. D') \
                             or starts-with(., 'Doctorat') or starts-with(., 'DPhil') \
                             or starts-with(., 'PhD') or starts-with(., '\nPh. D') \
                             or starts-with(., '\nPhD') or starts-with(., ' Ph.D') \
                             or starts-with(., '\nPh.D') or starts-with(., 'Ph.D') \
                             or starts-with(., 'D. Phil') or starts-with(., '\n      PhD') \
                             or starts-with(., '\n      Ph.D') or starts-with(., '(Ph.D') \
                             or starts-with(., '\n      DPhil') or starts-with(., 'D.Phil') \
                             or starts-with(., 'Doctor of Philosophy') or starts-with(., '\r\nPhD')\
                             or starts-with(., '\n      Dr. phil.') or starts-with(., '\r\nPh.D') \
                             or starts-with(., '\nDr. Phil.')]").extract()
        phd = phd[0].strip()
        prof['phd'] = phd
        yield prof
                
                


