# -*- coding: utf-8 -*-
import scrapy
from history_phd_data.items import HistoryPhdDataItem

class UcdavisspiderSpider(scrapy.Spider):
    name = 'UCDavisSpider'
    start_urls = ['https://history.ucdavis.edu/people/aanoosha',
                'https://history.ucdavis.edu/people/biagioli',
                'https://history.ucdavis.edu/people/dbiale',
                'https://history.ucdavis.edu/people/bossler',
                'https://history.ucdavis.edu/people/icampbel',
                'https://history.ucdavis.edu/people/hhchiang',
                'https://history.ucdavis.edu/people/geovet',
                'https://history.ucdavis.edu/people/crdecker',
                'https://history.ucdavis.edu/people/dickiner',
                'https://history.ucdavis.edu/people/gdowns',
                'https://history.ucdavis.edu/people/omniae',
                'https://history.ucdavis.edu/people/sfahren',
                'https://history.ucdavis.edu/people/eoconnor',
                'https://history.ucdavis.edu/people/qdjavers',
                'https://history.ucdavis.edu/people/rjeanbap',
                'https://history.ucdavis.edu/people/akelman',
                'https://history.ucdavis.edu/people/kyukim',
                'https://history.ucdavis.edu/people/jleroy',
                'https://history.ucdavis.edu/people/lgmaters',
                'https://history.ucdavis.edu/people/sjmckee',
                'https://history.ucdavis.edu/people/sgmiller',
                'https://history.ucdavis.edu/people/fzkolmst',
                'https://history.ucdavis.edu/people/lboropeza',
                'https://history.ucdavis.edu/people/jjperdez',
                'https://history.ucdavis.edu/people/rauchway',
                'https://history.ucdavis.edu/people/resendez',
                'https://history.ucdavis.edu/people/mtsaler',
                'https://history.ucdavis.edu/people/mschlott',
                'https://history.ucdavis.edu/people/ssen',
                'https://history.ucdavis.edu/people/jsmolens',
                'https://history.ucdavis.edu/people/rcstjohn',
                'https://history.ucdavis.edu/people/akharris',
                'https://history.ucdavis.edu/people/dstolz',
                'https://history.ucdavis.edu/people/fzgoofy',
                'https://history.ucdavis.edu/people/btezcan',
                'https://history.ucdavis.edu/people/cmtsu',
                'https://history.ucdavis.edu/people/cfwalker',
                'https://history.ucdavis.edu/people/lswarren',
                'https://history.ucdavis.edu/people/azientek'
                ]

    def parse(self, response):
        prof = HistoryPhdDataItem()
        prof['url'] = response.url
        prof['curr_university'] = 'University of California, Davis'
        prof['phd'] = response.xpath("//li//text()[starts-with(., 'Ph.D') or starts-with(., 'Ph. D') \
                             or starts-with(., 'Doctorat') or starts-with(., 'DPhil') \
                             or starts-with(., 'PhD') or starts-with(., '\nPh. D') \
                             or starts-with(., '\nPhD') or starts-with(., ' Ph.D') \
                             or starts-with(., '\nPh.D') or starts-with(., 'Ph.D') \
                             or starts-with(., 'D. Phil') or starts-with(., '\n      PhD') \
                             or starts-with(., '\n      Ph.D') or starts-with(., '(Ph.D') \
                             or starts-with(., '\n      DPhil') or starts-with(., 'D.Phil') \
                             or starts-with(., 'Doctor of Philosophy') \
                             or starts-with(., '\n      Dr. phil.')]").extract()
        yield prof
