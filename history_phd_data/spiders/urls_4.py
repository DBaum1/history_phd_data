# -*- coding: utf-8 -*-
import scrapy
from history_phd_data.items import HistoryPhdDataItem

class Urls4Spider(scrapy.Spider):
    name = 'urls_4'
    start_urls = ['https://history.rutgers.edu/people/faculty',
                  'https://history.tamu.edu/faculty/',
                  'https://arts-sciences.buffalo.edu/history/faculty/faculty-directory.html',
                  'https://history.ucsd.edu/people/faculty/index.html',
                  'https://dornsife.usc.edu/cf/hist/people/hist-faculty-roster.cfm',
                  'https://liberalarts.utexas.edu/history/faculty/',
                  'https://history.wustl.edu/people/88/'
                 ]
    
    def parse(self, response):
        prof = HistoryPhdDataItem()
        if response.url == 'https://history.rutgers.edu/people/faculty':
            prof['curr_university'] = 'Rutgers University–New Brunswick'
            hrefs = response.xpath("//div[@class='latestnews-items']//h4[@class='newstitle']//a/@href").extract()
            for href in hrefs:
                url = response.urljoin(href)
                request = scrapy.Request(url, callback=self.parse_rutgers)
                request.meta['prof'] = prof
                yield request
        elif response.url == 'https://history.tamu.edu/faculty/':
            prof['curr_university'] = 'Texas A&M University'
            for item in response.xpath("//*[@class='entry-content']//li"):
                prof['url'] = item.xpath("a//@href").extract()
                phd = item.xpath("text()").extract()
                phd = phd[0].strip()
                prof['phd'] = phd
                yield prof
        elif response.url == 'https://arts-sciences.buffalo.edu/history/faculty/faculty-directory.html':
            prof['curr_university'] = 'State University of New York at Buffalo'
            hrefs = response.xpath("//div[@class='profileinfo-teaser-name']//@href").extract()
            for href in hrefs:
                url = response.urljoin(href)
                request = scrapy.Request(url, callback=self.parse_buffalo)
                request.meta['prof'] = prof
                yield request
        elif response.url == 'https://history.ucsd.edu/people/faculty/index.html':
            prof['curr_university'] = 'University of California, San Diego'
            for item in response.xpath("//*[@class='profile-listing-card']"):
                prof['url'] = item.xpath("span//a//@href").extract()
                phd = item.xpath("span//text()[starts-with(., 'Ph.D') \
                    or starts-with(., ' Ph.D')]").extract()
                phd = phd[0].strip()                        
                prof['phd'] = phd
                yield prof
        elif response.url == 'https://dornsife.usc.edu/cf/hist/people/hist-faculty-roster.cfm':
            prof['curr_university'] = 'University of Southern California'
            for item in response.xpath("//*[@id='content']//div//tr"):
                prof['url'] = item.xpath("td[1]//a//@href").extract()
                phd = item.xpath("td//text()[2]").extract()
                phd = phd[0].strip()
                prof['phd'] = phd
                yield prof        
        elif response.url == 'https://liberalarts.utexas.edu/history/faculty/':
            prof['curr_university'] = 'University of Texas at Austin'
            hrefs = response.xpath("//*[@class='small-8 medium-9 large-10 columns faculty-contact-info']//h3//@href").extract()
            for href in hrefs:
                url = response.urljoin(href)
                request = scrapy.Request(url, callback=self.parse_ut)
                request.meta['prof'] = prof
                yield request
        else:
            prof['curr_university'] = 'Washington University in St. Louis'
            hrefs = response.xpath("//*[@class='container filtered-cards faculty-cards flex js-load-container']//@href").extract()
            for href in hrefs:
                url = response.urljoin(href)
                request = scrapy.Request(url, callback=self.parse_wustl)
                request.meta['prof'] = prof
                yield request
            
    def parse_rutgers(self, response):
        prof = response.meta['prof']    
        prof['url'] = response.url
        phd = response.xpath("//*[@class='field-value']//text() \
                    [starts-with(., 'Ph.D') or starts-with(., 'PhD')]").extract()
        phd = phd[0].strip()      
        prof['phd'] = phd
        yield prof
        
    def parse_buffalo(self, response):
        prof = response.meta['prof']   
        prof['url'] = response.url
        phd = response.xpath("//*[@class='text parbase section']//text() \
                    [starts-with(., 'PhD')]").extract()
        phd = phd[0].strip()
        prof['phd'] = phd
        yield prof

    def parse_ut(self, response):
        prof = response.meta['prof']    
        prof['url'] = response.url
        phd = response.xpath("//h3//span[@itemprop='alumniOf']//text()").extract()
        phd = phd[0].strip()
        prof['phd'] = phd
        yield prof

    def parse_wustl(self, response):
        prof = response.meta['prof']    
        prof['url'] = response.url
        phd = response.xpath("//div[@class='education']//text()[starts-with(., 'Ph.D') \
                             or starts-with(., 'Ph. D') or starts-with(., 'Doctorat') \
                             or starts-with(., 'DPhil') or starts-with(., 'PhD') \
                             or starts-with(., '\nPh. D') or starts-with(., '\nPhD') \
                             or starts-with(., ' Ph.D') or starts-with(., '\nPh.D') \
                             or starts-with(., 'Ph.D') or starts-with(., 'D. Phil') \
                             or starts-with(., '\n      PhD') or starts-with(., '\n      Ph.D') \
                             or starts-with(., '(Ph.D') or starts-with(., '\n      DPhil') \
                             or starts-with(., 'D.Phil') \
                             or starts-with(., 'Doctor of Philosophy') \
                             or starts-with(., '\n      Dr. phil.') \
                             or starts-with(., '\nDr. Phil.')]").extract()
        phd = phd[0].strip()
        prof['phd'] = phd
        yield prof